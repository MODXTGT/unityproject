using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

namespace Samarnggoon.GameDev3.Chapter2
 {
    public class BasicMouseInformation : MonoBehaviour {

        public Text m_TextMousePosition;
        public Text m_TextMouseScrollDelta;
        public Text m_TextMousePositionDelta;

        // Update is called once per frame
        void Update() {
            m_TextMousePosition.text = Input.mousePosition.ToString();
            m_TextMouseScrollDelta.text = Input.mousePosition.ToString();
            m_TextMousePositionDelta.text = Input.mousePosition.ToString();
        }
    }
}

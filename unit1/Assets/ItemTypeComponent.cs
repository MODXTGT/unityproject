using System.Collections;
using System.Collections.Generic;
using UnityEngine;

     public enum m_ItemType {
 COIN,
 BIGCOIN ,
POWERUP ,
POWERDOWN ,
}
public class ItemTypeComponent : MonoBehaviour
 {
    [SerializeField]
     protected ItemType m_ItemType;
 public ItemType Type
 {
     get
     {
         return m_ItemType;
         }
     set
     {
         m_ItemType = value;
        
        }
     }
 }
